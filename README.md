# Crypto-fm-test

This project provides a web application to check some of the most popular cryptocurrencies.

## Getting started

Firstly, you'll need to install the required node packages:

```bash
npm install
```

Once the installation has finished, you can run the project:

```bash
npm start
```
